-- credits: recursor's love2d series (youtube)

local Class = require "lib.Class"
local Vec2 = require "lib.Boxed.Vector2"

local Rect = Class:extends()

local abs = math.abs

function Rect:new(x, y, w, h)
	self.x = x or 0
	self.y = y or 0
	self.w = w or 0
	self.h = h or 0
end

function Rect.create_centered(x, y, w, h)
	local r = Rect(0, 0, w, h)
	r:set_center(x, y)
	return r
end

function Rect:center()
	return Vec2(self.x + self.w/2, self.y + self.h/2)
end

function Rect:set_center(x, y)
	self.x = x - self.w/2
	self.y = y - self.h/2
end

-- lower right corner
function Rect:max()
	return Vec2(self.x + self.w, self.y + self.h)
end

function Rect:min()
	return Vec2(self.x, self.y)
end

function Rect:size()
	return Vec2(self.w, self.h)
end

function Rect:minkowski_diff(other)
	local top_left = Vec2.sub(self:min(), other:max())
	local new_sz = Vec2.add(self:size(), other:size())
	local new_left = Vec2.add(top_left, Vec2.div_ret(new_sz, 2))
	return Rect.create_centered(new_left.x, new_left.y, new_sz.x, new_sz.y)
end

function Rect:closest_point_on_bounds(point)
	local min_dist = abs(point.x - self.x)
	local max = self:max()
	local bounds_point = Vec2(self.x, point.y)

	-- check x axis
	if abs(max.x - point.x) < min_dist then
		min_dist = abs(max.x - point.x)
		bounds_point = Vec2(max.x, point.y)
	end

	-- check y axis
	-- collide from top
	if abs(max.y - point.y) < min_dist then
		min_dist = abs(max.y - point.y)
		bounds_point = Vec2(point.x, max.y)
	end

	-- collide from bottom
	if abs(self.y - point.y) < min_dist then
		min_dist = abs(self.y - point.y)
		bounds_point = Vec2(point.x, self.y)
	end

	return bounds_point
end

function Rect.bounds_point_collision_side(bounds_point)
	local coll_info = { x = 0, y = 0 }

	if bounds_point.x > 0 then
		coll_info.x = -1
	elseif bounds_point.x < 0 then
		coll_info.x = 1
	end

	if bounds_point.y > 0 then
		coll_info.y = 1
	elseif bounds_point.y < 0 then
		coll_info.y = -1
	end

	return coll_info
end

return Rect