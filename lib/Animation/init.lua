local Class = require "lib.Class"
local YAML = require "lib.Yaml"
local State = require "lib.Animation.State"
local Anim = Class:extends()

function Anim:new(sheet, ply)
    if type(sheet) == "string" then
        local contents, err = love.filesystem.read(sheet)
        if contents then
            local cfg = YAML.eval(contents)
            ply.w = cfg.width
            ply.h = cfg.height
            self.cfg = cfg
        else
            assert(false, "Unable to open template: " .. err)
        end
    end

    self.ply = ply
    self.sheet = sheet
    self.states = {}
    self.state = false
    self.flip = false

    if self.cfg then
        self.sheet = love.graphics.newImage(self.cfg.spritesheet)
        if self.cfg.states ~= nil then
            for k, v in pairs(self.cfg.states) do
                local s = self.cfg.states[k]
                local hsh = { {}, 0, {}, nil }
                for i=1, #s.frames do
                    hsh[1][i] = s.frames[i]
                end
                hsh[2] = s.fps
                hsh[3] = { x = s.offset[1], y = s.offset[2] }
                hsh[4] = s.callback ~= nil and s.callback or nil
                self:add_animation(k, unpack(hsh))

                if s.rotate then self.states[k].rotate = true; self.states[k].rotate_deg = 0 end
            end
            self.state = self.states[self.cfg.initial]
        end
    end
end

function Anim:add_animation(state, frames, fps, offsets, cb)
    self.states[state] = State(self.sheet, state, frames, fps, offsets, cb)
end

function Anim:has_state(state)
    return self.states[state] ~= nil
end

function Anim:set_rotate(state)
    self.states[state].rotate = true
end

function Anim:set_state(state)
    assert(self.states[state], "No such animation state exists: " .. state)

    if not self.state then
        self.state = self.states[1]

    elseif self.state.name ~= state then
        self.state.pos = 1
        self.state.paused = false
        if self.state.rotate then self.state.rotate_deg = 0 end
    end

    self.state = self.states[state]
end

function Anim:pause()
    self.state.paused = true
end

function Anim:update(dt)
    if self.state.fps > 0 and not self.state.paused then
        self.state.t = self.state.t - dt
        if self.state.t <= 0 then
            if self.state.rotate then
                if self.flip then
                    self.state.rotate_deg = self.state.rotate_deg-30
                else
                    self.state.rotate_deg = self.state.rotate_deg+30
                end
                if self.state.rotate_deg >= 360 then self.state.rotate_deg = 0 end
            end

            self.state.pos = self.state.pos + 1
            self.state.t = 1 / self.state.fps
            if self.state.pos > #self.state.quads then
                if self.state.cb then
                    local cb = self.state.cb
                    if type(cb) == "string" then self.ply[cb](self.ply, self)
                    else self.state.cb(self) end
                end
                if not self.state.paused then self.state.pos = 1 else self.state.pos = #self.state.quads end
            end
        end
    end
end

function Anim:draw()
    local rot, flipV = 0, 1
    local xoff, yoff = self.flip and self.state.offset.x+self.ply.w or self.state.offset.x,
                        self.state.offset.y and self.state.offset.y or 0
    
    if self.state.rotate then
        rot = math.rad(self.state.rotate_deg)
        local _, _, sw, sh = self.state.quads[self.state.pos]:getViewport()
        xoff = (sw+self.state.offset.x) / 2
        yoff = (sh+self.state.offset.y) / 2
    end

    love.graphics.draw(
        self.sheet,
        self.state.quads[self.state.pos],
        self.ply.x-self.ply.w/2,
        self.ply.y-self.ply.h/2,
        rot, -- rotate
        self.flip and -1 or 1, -- flip h
        flipV, -- flip v
        xoff, -- xoffset
        yoff -- yoffset
    )
end

return Anim