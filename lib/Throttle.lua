local min_dt = 1/60
local next_time = love.timer.getTime()
local cur_time


local Throttle = {}

function Throttle.update(dt)
    next_time = next_time + min_dt
end

function Throttle.draw()
    cur_time = love.timer.getTime()
    if next_time <= cur_time then
      next_time = cur_time
      return
    end
    love.timer.sleep(next_time - cur_time)
end

return Throttle