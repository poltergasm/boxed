local Class = require "lib.Class"
local Object = require "lib.Boxed.Object"
local Vec2 = require "lib.Boxed.Vector2"
local Rect = require "lib.Boxed.Rect"

local Boxed = Class:extends()


local function exists(tbl, n)
    for idx, val in ipairs(tbl) do
        if val == n then return true end
    end

    return false
end

function Boxed:new(tilesz, gravity)
	self.opts = {
		objects = {},
		tilesz = tilesz,
		gravity = gravity*tilesz
	}
end

function Boxed:add_rect(t, x, y, w, h, m)
	m = m ~= nil and m/self.opts.gravity or 1
	local obj = Object(self, t, x, y, w, h, m)
	table.insert(self.opts.objects, obj)
	return obj
end

function Boxed:destroy()
	for i=1, #self.opts.objects do
		self:remove(self.opts.objects[i])
	end
	self.opts = {}
	self = nil
end

function Boxed:remove(e)
	for i=1, #self.opts.objects do
		if self.opts.objects[i] == e then
			table.remove(self.opts.objects, i)
		end
	end
end

function Boxed:update(dt)
	for i=1, #self.opts.objects do
		self.opts.objects[i]:update(dt)
	end
end

-- rect1, rect2
function Boxed:rect_intersects(r1, r2)
	if r1.x < r2.x+r2.w and
		r2.x < r1.x+r1.w and
		r1.y < r2.y+r2.h and
		r2.y < r1.y+r1.h then
			return true
	end
end

function Boxed:move(obj, rect)
	local colliders = {}
	for i=1, #self.opts.objects do
		local obj2 = self.opts.objects[i]
		if obj ~= obj2 then
			--if not obj2.kinematic then
				if self:rect_intersects(rect, obj2.rect) then
					local md = obj2.rect:minkowski_diff(rect)
					local sep = md:closest_point_on_bounds(Vec2())
				    table.insert(colliders, { item = obj2, sep = sep })
	        	end
	        --end
		end
	end

	return colliders
end

function Boxed:query_rect(x, y, w, h)
	local rect = Rect.create_centered(x, y, w, h)
	local colliders = {}
	for i=1, #self.opts.objects do
		local item = self.opts.objects[i]
		if self:rect_intersects(rect, item.rect) then
			table.insert(colliders, item)
		end
	end

	return colliders, rect
end

return Boxed