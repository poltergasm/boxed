local Class = require "lib.Class"
local Vec2 = require "lib.Boxed.Vector2"
local Rect = require "lib.Boxed.Rect"

local Object = Class:extends()

local abs = math.abs

function Object:new(world, t, x, y, w, h, m)
	self.pos = Vec2(x, y)
	self.vel = Vec2(0, 0)
	self.rect = Rect(x, y, w, h)
	self.dim = { w = w, h = h }
	self.mass = m
	self.type = t
	self.grounded = false
	self.world = world
	self.bounciness = 0
	self.force = Vec2(0, 0)
	self:set_friction(1.8)
	self.gravity = world.opts.gravity
	self.jelly = false
	self.alive = true
	self.speed = 0
	self.prev_speed = 0
	self.stopped = false
	self.sensor = false
	self.apply_force = { x = 0, y = 0 }
	self.on_platform = false
	self.kinematic = false

	if self.type == "kinematic" then
		self.type = "dynamic"
		self.kinematic = true
	end
end

function Object:get_points() return self.rect.x, self.rect.y, self.rect.w, self.rect.h end
function Object:get_velocity() return self.vel.x, self.vel.y end

function Object:change_velocity_by_collision_normal(nx, ny)
    bounciness = bounciness or 0
    local vx, vy = self.vel.x, self.vel.y
    
    if (nx < 0 and vx > 0) or (nx > 0 and vx < 0) then
        vx = vx * self.bounciness
    end
    
    if (ny < 0 and vy > 0) or (ny > 0 and vy < 0) then
        vy = -vy * self.bounciness
    end
    
    self.vel.x, self.vel.y = vx, vy
end

function Object:set_friction(n) self.friction = n*255 end
function Object:set_bounce(n) self.bounciness = n end
function Object:set_id(id) self.id = id end
function Object:set_vert_velocity(n) self.vel.y = n end
function Object:set_horiz_velocity(n) self.speed = n end
function Object:get_type() return self.type end
function Object:set_type(t) self.type = t end -- TODO: Check type
function Object:get_id() return self.id end
function Object:set_gravity(n) self.gravity = n end
function Object:get_position() return self.rect.x+self.rect.w/2, self.rect.y+self.rect.h/2 end
function Object:getX() return self.rect.x+self.rect.w/2 end
function Object:getY() return self.rect.y+self.rect.h/2 end
function Object:set_sensor(b) self.sensor = b end
function Object:get_dimensions() return self.rect.w, self.rect.h end
function Object:set_jelly(b) self.jelly = b end
function Object:destroy()
	self.alive = false
	self.world:remove(self)
end

function Object:check_brake(dt)
	local brake = dt * (self.vel.x < 0 and self.friction or -self.friction)
    if not self.grounded then brake = 0 end
    if abs(brake) > abs(self.vel.x) then
        self.vel.x = 0
        self.speed = 0
        self.stopped = false
    else
        self.vel.x = self.vel.x + brake
    end
end

function Object:update(dt)
	if self.type == "dynamic" then
		if self.kinematic or self.apply_force.y ~= 0 then
			self.vel.y = self.vel.y + self.apply_force.y
			next_y = self.rect.y + self.vel.y * dt
		else
			self.vel.y = self.vel.y + (self.gravity) * self.mass
			next_y = self.rect.y + self.vel.y * dt
		end

		if self.apply_force.x ~= 0 or self.kinematic then
			if self.apply_force.x > 0 then
				self.vel.x = (not self.grounded and self.apply_force.x or self.vel.x) + dt-- * (not self.grounded and 0 or -self.friction)
				if self.vel.x >= self.apply_force.x then self.apply_force.x = 0 end
			elseif self.apply_force.x < 0 then
				self.vel.x = (not self.grounded and self.apply_force.x or self.vel.x) - dt-- * (not self.grounded and 0 or -self.friction)
				if self.vel.x <= self.apply_force.x then self.apply_force.x = 0 end
			end
		else

			if self.speed > 0 then
				self.vel.x = self.vel.x + dt * ((self.vel.x < 0 and self.grounded) and self.friction or self.speed)
				if self.vel.x > self.speed then self.vel.x = self.speed end
				--self.vel.x = (not self.grounded and self.vel.x-(self.mass*2) or self.vel.x) + dt-- * (not self.grounded and 0 or -self.friction)
				--if self.vel.x < 0 then self:check_brake(dt) end
				--self:check_brake(dt)
			elseif self.speed < 0 then
				self.vel.x = self.vel.x + dt * ((self.vel.x > 0 and self.grounded) and -self.friction or self.speed)
				if self.vel.x < self.speed then self.vel.x = self.speed end
				--self.vel.x = (not self.grounded and self.vel.x-(self.mass*2) or self.vel.x) - dt-- * (not self.grounded and 0 or -self.friction)
				--if self.vel.x > 0 then self:check_brake(dt) end
				--self:check_brake(dt)
			else
				self:check_brake(dt)
			end
		end

		--if self.stopped or (self.prev_speed ~= self.speed) then self:check_brake(dt) end

		self.grounded = false
		self.speed = 0

		next_x = self.rect.x + self.vel.x * dt

		local colls = self.world:move(self, Rect(next_x, next_y, self.rect.w, self.rect.h))

		local receiver = type(self.id) == "table" and self.id or self
		if #colls > 0 then
			for i=1, #colls do
				local contact = true
				local normal = Rect.bounds_point_collision_side(colls[i].sep)
				if self.presolve_contact ~= nil then contact = self.presolve_contact(receiver, normal, colls[i].item) end
				-- if slide
				if contact then
	
					if normal.y == -1 then
						if colls[i].item.jelly then
							self.vel.y = self.vel.y + colls[i].sep.y
						else
							--self.vel.y = colls[i].item.vel.y
							next_y = next_y + colls[i].sep.y
							--[[if colls[i].item.vel.y ~= 0 then
								self.vel.y = colls[i].item.vel.y
								self.on_platform = true
							end]]
							self.grounded = true
						end
					end
					if normal.x ~= 0 then

						next_x = next_x + colls[i].sep.x

						local o = colls[i].item
						-- can we push it?
						if o:get_type() == "dynamic" then
							self.vel.x = self.vel.x / (o.mass*10/2)
							o.vel.x = self.vel.x
						else
							self.vel.x = 0
						end
					end

					self:change_velocity_by_collision_normal(normal.x, normal.y)
					if self.contact ~= nil then self.contact(receiver, normal, colls[i].item) end
				end
			end
		end
		
		self.rect.x, self.rect.y = next_x, next_y
	end
end

function Object:apply_force(vx, vy)
	self.vel.x = vx
	self.vel.y = vy == 0 and self.vel.y or vy --vy < 0 and -1.4 or self.vel.y
	self.force.x = self.rect.x + vx
	self.force.y = self.rect.y + vy
end

function Object:draw_rect()
	if self.alive then
		love.graphics.rectangle("line", self.rect.x, self.rect.y, self.rect.w, self.rect.h)
	end
end

return Object