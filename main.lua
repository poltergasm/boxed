
local sw, sh = love.window.getMode()
SCREEN_WIDTH, SCREEN_HEIGHT = sw/4, sh/4

love.graphics.setDefaultFilter("nearest", "nearest")
local Res = require "lib.TLfres"
local Boxed = require "lib.Boxed"
local Throttle = require "lib.Throttle"

local Player = require "ents.Player"
local ArrowShooter = require "ents.ArrowShooter"

local lg, la = love.graphics, love.audio

sfx = {
	sword = la.newSource("res/sword.wav", "static"),
	defeat = la.newSource("res/defeat.wav", "static")
}

local e1, e2, e4, player, box, arrow_shooter
local running = false


local function hex2rgb(hex)
    hex = hex:gsub("#","")
    return tonumber("0x"..hex:sub(1,2)), tonumber("0x"..hex:sub(3,4)), tonumber("0x"..hex:sub(5,6))
end

function get_color(hex)
    local r,g,b = hex2rgb(hex)
    return r/255, g/255, b/255
end

local ply_color = { get_color('2272f3') }
local plat_color = { get_color('22f338') }
local back_color = { get_color('353438') }
local floor_color = { get_color('090713') }

local background = lg.newImage("res/background.png")

function love.load()
	boxed = Boxed(16, 2.4)

	e1 = boxed:add_rect("static", 0, SCREEN_HEIGHT-16, SCREEN_WIDTH, 16)
	e2 = boxed:add_rect("dynamic", 24, 24, 16, 32, 8)

	--e1:set_jelly(true)
	e4 = boxed:add_rect("static", 80, SCREEN_HEIGHT-48, 64, 7)
	e4:set_id("Oneway")
	e4.is_ice = true

	e1.is_platform = true
	e2.is_platform = true
	e4.is_platform = true

	box = boxed:add_rect("dynamic", 180, 24, 16, 16, 8)
	box.is_platform = true
	box:set_bounce(0.6)

	box.hits = 3
	box.presolve_contact = function(item, normal, other)
		if other:get_id() == "Player" then return false
		else return true end
	end

	box2 = boxed:add_rect("dynamic", 180, 0, 16, 16, 8)
	box2.is_platform = true
	box2:set_bounce(0.6)

	box2.hits = 3
	box2.presolve_contact = function(item, normal, other)
		if other:get_id() == "Player" then return false
		else return true end
	end

	-- arrow shooter

	player = Player(24, 0)
	--arrow_shooter = ArrowShooter(260, SCREEN_HEIGHT-48)
end

function love.update(dt)
	Throttle:update(dt)
	if running then
		boxed:update(dt)
		player:update(dt)
		--arrow_shooter:update(dt)
	end
end

function love.draw()
	Res.beginRendering(SCREEN_WIDTH, SCREEN_HEIGHT)
	lg.clear(0, 0, 0)
	lg.setColor(1, 1, 1, 1)
	lg.draw(background, 0, 0)

	lg.setColor(1, 1, 1, 1)
	lg.rectangle("line", e2:get_points())
	lg.setColor(ply_color)
	lg.rectangle("fill", e4:get_points())
	
	lg.setColor(ply_color)
	box:draw_rect()
	box2:draw_rect()

	lg.setColor(1, 1, 1, 1)
	--arrow_shooter:draw()
	player:draw()
	

	lg.setColor(floor_color)
	lg.rectangle("fill", e1:get_points())

	Res.endRendering()

	Throttle:draw()
end

function love.keypressed(key)
	if key == "space" then running = true
	elseif key == "x" and not player.attacking then
		player.jumping = true
		player.spr:set_state("jump")
		player.collider:set_vert_velocity(-180)
	elseif key == "z" and not player.attacking then
		sfx.sword:stop()
		sfx.sword:play()
		player.attacking = true
		player.spr:set_state("attack")
		player:attack()
	end
end