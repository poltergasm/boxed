function love.conf(t)
    t.window.width = 320*4
    t.window.height = 208*4
    t.window.vsync = false
    t.modules.physics = false
    t.window.fullscreen = false
end