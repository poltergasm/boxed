local Class = require "lib.Class"
local Anim = require "lib.Animation"

local Player = Class:extends()

local knight = love.graphics.newImage("res/knight.png")
local light = love.graphics.newImage("res/lightdot.png")

function Player:new(x, y)
	self.is_player = true
	self.w = 12
	self.h = 18
	self.dir = 1
	self.x, self.y = x+self.w/2, y+self.h/2

	self.spr = Anim(knight, self)
	self.spr:add_animation("idle", {
		{ 0, 0, 64, 64 },
		{ 64, 0, 64, 64 },
		{ 128, 0, 64, 64 },
		{ 192, 0, 64, 64 },
		{ 256, 0, 64, 64 },
	}, 6, { x = 26, y = 30 })

	self.spr:add_animation("walk", {
		{ 0, 64, 64, 64 },
		{ 64, 64, 64, 64 },
		{ 128, 64, 64, 64 },
		{ 192, 64, 64, 64 },
		{ 256, 64, 64, 64 },
		{ 320, 64, 64, 64 },
		{ 384, 64, 64, 64 },
		{ 448, 64, 64, 64 }
	}, 14, { x = 26, y = 30 })

	self.spr:add_animation("jump", {
		{ 0, 128, 64, 64 },
		{ 64, 128, 64, 64 },
		{ 128, 128, 64, 64 },
	}, 12, { x = 26, y = 30 }, function(a) self.jumping = false; a:set_state("fall") end)

	self.spr:add_animation("fall", {
		{ 0, 192, 64, 64 },
		{ 64, 192, 64, 64 },
	}, 12, { x = 26, y = 30 })

	self.spr:add_animation("attack", {
		{ 0, 256, 64, 64 },
		{ 64, 256, 64, 64 },
		{ 128, 256, 64, 64 },
		{ 192, 256, 64, 64 },
		{ 256, 256, 64, 64 },
		{ 320, 256, 64, 64 },
	}, 18, { x = 26, y = 30 }, function(a) self.attacking = false; a:set_state("idle") end)

	self.spr:set_state("idle")

	self.collider = boxed:add_rect("dynamic", x, y, self.w, self.h, 8)
	self.collider:set_friction(1.2)
	self.collider:set_id("Player")

	self.collider.presolve_contact = function(item, normal, other)
		if other.id == "Oneway" then
			if item.rect.y > other.rect.y-(item.rect.h) or normal.x ~= 0 then
				return false
			end
		end

		return true
	end

	self.collider.contact = function(item, normal, other)
		if other.is_platform and normal.y == -1 then
			if other.is_ice then self.collider:set_friction(0.2)
			else self.collider:set_friction(1) end
			self.grounded = true
			self.jumping = false
		end
	end
end

function Player:attack()
	local colls = self.collider.world:query_rect(self.dir == 1 and (self.x+16) or self.x-16, self.y-4, 16, 24)
	if #colls > 0 then
		for i=1, #colls do
			local b = colls[i]
			if b.hits then
				b.hits = b.hits-1
				b.vel.y = -140
				b.vel.x = self.dir == 1 and 20 or -20
				if b.hits <= 0 then
					sfx.defeat:stop()
					sfx.defeat:play()
					self:do_particle(b.rect.x, b.rect.y)
					b:destroy()
				end
			end
		end
	end
end

function Player:do_particle(x, y)
	self.particle = { x = x, y = y }
	self.ps = love.graphics.newParticleSystem(light, 24)
	--self.ps:setColors(1, 0.1704545468092, 0.1704545468092, 0.81818181276321, 1, 0.18560606241226, 0.18560606241226, 0.80681818723679, 1, 0.19318181276321, 0.19318181276321, 0.81060606241226, 1, 0.18939393758774, 0.18939393758774, 0)
	self.ps:setColors(0.11372549019608, 0.27058823529412, 0.80392156862745, 1)
	self.ps:setDirection(-1.5707963705063)
	self.ps:setEmissionArea("none", 0, 0, 0, false)
	self.ps:setEmissionRate(87.894798278809)
	self.ps:setEmitterLifetime(0.5)
	self.ps:setInsertMode("top")
	self.ps:setLinearAcceleration(0, 0, 0, 0)
	self.ps:setLinearDamping(0.46602809429169, 0.10267757624388)
	--self.ps:setOffset(75, 75)
	self.ps:setParticleLifetime(0.32856824994087, 0.55900079011917)
	self.ps:setRadialAcceleration(233.01403808594, 875.86492919922)
	self.ps:setRelativeRotation(false)
	self.ps:setRotation(0, 0)
	--self.ps:setSizes(0.096675217151642)
	--self.ps:setSizeVariation(0)
	self.ps:setSpeed(-2.3485796451569, 77.037292480469)
	self.ps:setSpin(0, 0)
	self.ps:setSpinVariation(0)
	self.ps:setSpread(5.3032388687134)
	self.ps:setTangentialAcceleration(42.798496246338, 28.047088623047)
end

function Player:update(dt)
	--self.sword_box = nil
	if self.ps then self.ps:update(dt) end
	self.x, self.y = self.collider:get_position()
	self.spr:update(dt)

	local vx, vy = self.collider:get_velocity()
	if vy ~= 0 and not self.jumping and not self.attacking then
		self.spr:set_state("fall")
	end

	if not self.jumping and not self.attacking then
		if vx ~= 0 and self.grounded then
			self.spr:set_state("walk")
		elseif vx == 0 and self.grounded then
			self.spr:set_state("idle")
		end
	end

	if love.keyboard.isDown("right") then
		self.collider:set_horiz_velocity(160)
		self.spr.flip = false
		self.dir = 1
	elseif love.keyboard.isDown("left") then
		self.collider:set_horiz_velocity(-160)
		self.spr.flip = true
		self.dir = -1
	end

	self.grounded = false
end

function Player:draw()
	if self.ps then love.graphics.draw(self.ps, self.particle.x, self.particle.y) end
	self.spr:draw()
	--self.collider:draw_rect()
end

return Player