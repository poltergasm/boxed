local Class = require "lib.Class"

local ArrowShooter = Class:extends()

local sfx = {
	arrow_hit = love.audio.newSource("res/arrow_hit.wav", "static"),
	boom = love.audio.newSource("res/boom.wav", "static"),
	shoot = love.audio.newSource("res/shoot.wav", "static"),
}
function ArrowShooter:new(x, y)
	self.w = 16
	self.h = 32
	self.dir = 1
	self.x, self.y = x+self.w/2, y+self.h/2
	self.color = { get_color('9e2835') }

	self.collider = boxed:add_rect("static", x, y, self.w, self.h, 10)
	self.collider.is_platform = true

	self.shooter = boxed:add_rect("static", x-8, y+self.h/2, 8, 8, 1)
	self.shooter.is_platform = true
	self.shooter:set_id("Oneway")
	self.shoot_time = love.timer.getTime()
	self.arrow = false
end

function ArrowShooter:new_arrow()
	sfx.shoot:stop()
	sfx.shoot:play()
	local x, y = self.shooter:get_position()
	self.arrow = boxed:add_rect("dynamic", x-12, y-2, 8, 4, 8)
	self.arrow.hits = 1
	--self.arrow:set_gravity(0)
	self.arrow:set_vert_velocity(-160)
	self.arrow.contact = function(item, normal, other)
		--sfx.arrow_hit:stop()
		--sfx.arrow_hit:play()
		item.hit = true
		--item:destroy()
		if normal.x ~= 0 then item:set_vert_velocity(-80) end
		other:set_vert_velocity(-100)
	end
end

function ArrowShooter:update(dt)
	self.draw_boom = false
	local gt = love.timer.getTime()
	if gt > self.shoot_time+4 then
		self.shoot_time = gt
		self:new_arrow()
	end

	if self.arrow and not self.arrow.hit then
		self.arrow:set_horiz_velocity(-140)
	end

	if self.arrow and self.arrow.vel.x == 0 and not self.arrow.explode then
		local ax, ay = self.arrow:get_position()
		ax = ax
		ay = ay
		sfx.boom:stop()
		sfx.boom:play()
		self.arrow.explode = true
		self.arrow:destroy()
		
		local colls, rect = self.collider.world:query_rect(ax, ay, 128, 128)
		self.draw_boom = rect 
		if #colls > 0 then
			for i=1, #colls do
				if colls[i]:get_type() == "dynamic" then
					colls[i]:set_vert_velocity(-100)
					local cx, cy = colls[i]:get_position()
					colls[i]:set_horiz_velocity(cx < ax and -160 or 160)
				end
			end
		end
	end
end

function ArrowShooter:draw()
	love.graphics.setColor(self.color)
	love.graphics.rectangle("fill", self.collider:get_points())
	love.graphics.rectangle("fill", self.shooter:get_points())

	if self.draw_boom then
		love.graphics.rectangle("line", self.draw_boom.x, self.draw_boom.y, self.draw_boom.w, self.draw_boom.h)
	end

	love.graphics.setColor(1, 1, 1, 1)
	if self.arrow then self.arrow:draw_rect() end
end

return ArrowShooter